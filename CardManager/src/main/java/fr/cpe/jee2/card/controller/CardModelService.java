package fr.cpe.jee2.card.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import fr.cpe.jee2.card.constantes.ConstanteCard;
import fr.cpe.jee2.card.dto.CardModelDTO;
import fr.cpe.jee2.card.dto.CardReferenceDTO;
import fr.cpe.jee2.card.model.CardModel;
import fr.cpe.jee2.card.model.CardReference;
import fr.cpe.jee2.dto.EnveloppeDTO;
import fr.cpe.jee2.dto.constantes.ConstanteUser;
import fr.cpe.jee2.emitter.controller.BusService;
import fr.cpe.jee2.store.constantes.ConstanteStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CardModelService {
	
	private Random rand;

	public CardModelService() {
		this.rand=new Random();
	}


	@Autowired
	private CardModelRepository cardRepository;
	
	@Autowired
	private CardReferenceService cardRefService;

	@Autowired
	private BusService busService;

	public List<CardModel> getAllCardModel() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public void addCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}

	public void updateCardRef(CardModel cardModel) {
		cardRepository.save(cardModel);

	}
	public void updateCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}
	public Optional<CardModel> getCard(Integer id) {
		return cardRepository.findById(id);
	}
	
	public void deleteCardModel(Integer id) {
		cardRepository.deleteById(id);
	}
	
	public List<CardModel> getRandCard(int nbr){
		List<CardModel> cardList=new ArrayList<>();
		for(int i=0;i<nbr;i++) {
			CardReference currentCardRef=cardRefService.getRandCardRef();
			CardModel currentCard=new CardModel(currentCardRef);
			currentCard.setAttack(rand.nextFloat()*100);
			currentCard.setDefence(rand.nextFloat()*100);
			currentCard.setEnergy(100);
			currentCard.setHp(rand.nextFloat()*100);
			currentCard.setPrice(111);
			//save new card before sending for user creation
			//this.addCard(currentCard);
			cardList.add(currentCard);
		}
		return cardList;
	}

	public void getRandCardActiveMQ(int nbr, String reponse){
		List<CardModelDTO> cardList=new ArrayList<>();
		for(int i=0;i<nbr;i++) {
			CardReference cardReference= cardRefService.getRandCardRef();
			CardModelDTO cardModelDTO =new CardModelDTO(cardReference.toCardReferenceDTO());
			cardModelDTO.setAttack(rand.nextFloat()*100);
			cardModelDTO.setDefence(rand.nextFloat()*100);
			cardModelDTO.setEnergy(100);
			cardModelDTO.setHp(rand.nextFloat()*100);
			cardModelDTO.setPrice(111);
			//save new card before sending for user creation
			//this.addCard(currentCard);
			cardList.add(cardModelDTO);
		}

		//On envoie la réponse
		EnveloppeDTO enveloppeDTO = new EnveloppeDTO();
		enveloppeDTO.setMethodName("Retour fonction random card");
		enveloppeDTO.addParameters(ConstanteCard.RETOUR_LIST_RANDOM_CARD, cardList);
		//On route vers la reponse vers le bon bus
		if(ConstanteUser.ID_APPLICATION.equals(reponse)) {
			busService.sendMsg(enveloppeDTO, ConstanteUser.QUEUE_NAME);
		}
		if(ConstanteStore.ID_APPLICATION.equals(reponse)){
			busService.sendMsg(enveloppeDTO, ConstanteStore.QUEUE_NAME);
		}

	}


	public List<CardModel> getAllCardToSell(){
		return this.cardRepository.findByUser(null);
	}
}

