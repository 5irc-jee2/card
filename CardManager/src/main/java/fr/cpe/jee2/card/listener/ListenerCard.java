package fr.cpe.jee2.card.listener;

import fr.cpe.jee2.card.constantes.ConstanteCard;
import fr.cpe.jee2.card.controller.CardModelService;
import fr.cpe.jee2.dto.EnveloppeDTO;
import fr.cpe.jee2.receiver.controller.BusListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ListenerCard extends BusListener {

    @Autowired
    CardModelService cardModelService;

    @Override
    public void parseData(EnveloppeDTO pEnveloppeDTO) {

        System.out.println(pEnveloppeDTO.toString());
        switch(pEnveloppeDTO.getMethodName()){
            case ConstanteCard.METHOD_RANDOM_CARD:
                if(pEnveloppeDTO.getParameters().get(ConstanteCard.PARAM_RAND_CARD_NB) instanceof Integer &&
                pEnveloppeDTO.getParameters().get(ConstanteCard.PARAM_APPLICATION_REPONSE) instanceof String) {
                    cardModelService.getRandCardActiveMQ(
                            (int) pEnveloppeDTO.getParameters().get(ConstanteCard.PARAM_RAND_CARD_NB),
                            (String) pEnveloppeDTO.getParameters().get(ConstanteCard.PARAM_APPLICATION_REPONSE));
                } else {
                    System.out.println("Mauvais paramètre");
                }
                break;
            default:
                System.out.println("Aucune méthode parse");
                break;
        }
    }
}
