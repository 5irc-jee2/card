package fr.cpe.jee2.card.controller;

import fr.cpe.jee2.card.model.CardReference;
import org.springframework.data.repository.CrudRepository;


public interface CardRefRepository extends CrudRepository<CardReference, Integer> {

}
