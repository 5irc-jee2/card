package fr.cpe.jee2.card.controller;

import fr.cpe.jee2.dto.UserModelDTO;
import fr.cpe.jee2.card.model.CardModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    List<CardModel> findByUser(UserModelDTO u);
}
