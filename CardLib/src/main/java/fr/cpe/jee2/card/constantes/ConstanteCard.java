package fr.cpe.jee2.card.constantes;

public class ConstanteCard {
    public static final String QUEUE_NAME = "queue_card";

    public static final String METHOD_RANDOM_CARD = "randomCard";

    public static final Integer PARAM_RAND_CARD_NB = 1;

    public static final Integer PARAM_APPLICATION_REPONSE = 2;

    public static final Integer RETOUR_LIST_RANDOM_CARD = 1;
}
