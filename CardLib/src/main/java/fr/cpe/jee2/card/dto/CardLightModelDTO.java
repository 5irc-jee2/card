package fr.cpe.jee2.card.dto;

public class CardLightModelDTO extends CardReferenceDTO {

    private float energy;
    private float hp;
    private float defence;
    private float attack;
    private float price;

    private Integer userId;
    private Integer storeId;

    public CardLightModelDTO() {

    }

    public CardLightModelDTO(CardModelDTO cModel) {
        super(cModel);
        this.energy=cModel.getEnergy();
        this.hp=cModel.getHp();
        this.defence=cModel.getDefence();
        this.attack=cModel.getAttack();
        this.price=cModel.getPrice();
        this.userId = cModel.getUserId();
        this.storeId = cModel.getStoreId();
    }

    public float getEnergy() {
        return energy;
    }

    public float getHp() {
        return hp;
    }

    public float getDefence() {
        return defence;
    }

    public float getAttack() {
        return attack;
    }

    public float getPrice() {
        return price;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder("CardLightModelDTO{");
        stringBuilder.append("energy=").append(energy);
        stringBuilder.append(", hp=").append(hp);
        stringBuilder.append(", defence=").append(defence);
        stringBuilder.append(", attack=").append(attack);
        stringBuilder.append(", price=").append(price);
        stringBuilder.append(", userId=").append(userId);
        stringBuilder.append(", storeId=").append(storeId);
        stringBuilder.append('}');
        return stringBuilder.toString();
    }
}
