package fr.cpe.jee2.card.dto;

public class CardModelDTO extends CardReferenceDTO {

	private float energy;
	private float hp;
	private float defence;
	private float attack;
	private float price;

	private Integer userId;

	private Integer storeId;

	public CardModelDTO() {
		super();
	}

	public CardModelDTO(CardReferenceDTO cardRef) {
		super(cardRef);
	}

	public CardModelDTO(String name, String description, String family, String affinity, float energy, float hp,
						float defence, float attack, String imgUrl, String smallImg, float price, Integer userId, Integer storeId) {
		super(name, description, family, affinity,imgUrl,smallImg);
		this.energy = energy;
		this.hp = hp;
		this.defence = defence;
		this.attack = attack;
		this.price = price;
		this.userId = userId;
		this.storeId = storeId;
	}

	public float getEnergy() {
		return energy;
	}
	public float getHp() {
		return hp;
	}
	public float getDefence() {
		return defence;
	}
	public float getAttack() {
		return attack;
	}
	public float getPrice() {
		return price;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(final Integer pUserId) {
		userId =  pUserId;
	}

	public Integer getStoreId() {
		return storeId;
	}

	public void setEnergy(float energy) {
		this.energy = energy;
	}

	public void setHp(float hp) {
		this.hp = hp;
	}

	public void setDefence(float defence) {
		this.defence = defence;
	}

	public void setAttack(float attack) {
		this.attack = attack;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public void setStoreId(Integer storeId) {
		this.storeId = storeId;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("CardModelDTO{");
		stringBuilder.append("energy=").append(energy);
		stringBuilder.append(", hp=").append(hp);
		stringBuilder.append(", defence=").append(defence);
		stringBuilder.append(", attack=").append(attack);
		stringBuilder.append(", price=").append(price);
		stringBuilder.append(", userId=").append(userId);
		stringBuilder.append(", storeId=").append(storeId);
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
