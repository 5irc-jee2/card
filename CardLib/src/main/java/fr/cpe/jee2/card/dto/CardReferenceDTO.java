package fr.cpe.jee2.card.dto;

import java.io.Serializable;

public class CardReferenceDTO implements Serializable {

	private Integer id;
	private String name;
	private String description;
	private String family;
	private String affinity;
	private String imgUrl;
	private String smallImgUrl;

	public CardReferenceDTO() {

	}

	public CardReferenceDTO(CardReferenceDTO c) {
		super();
		this.id=c.getId();
		this.name = c.getName();
		this.description = c.getDescription();
		this.family = c.getFamily();
		this.affinity = c.getAffinity();
		this.imgUrl=c.getImgUrl();
		this.smallImgUrl=c.getSmallImgUrl();
	}

	public CardReferenceDTO(String name, String description, String family, String affinity, String imgUrl, String smallImgUrl) {
		super();
		this.name = name;
		this.description = description;
		this.family = family;
		this.affinity = affinity;
		this.smallImgUrl=smallImgUrl;
		this.imgUrl=imgUrl;
	}

	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public String getFamily() {
		return family;
	}
	public String getAffinity() {
		return affinity;
	}
	public Integer getId() {
		return id;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public String getSmallImgUrl() {
		return smallImgUrl;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public void setSmallImgUrl(String smallImgUrl) {
		this.smallImgUrl = smallImgUrl;
	}

	@Override
	public String toString() {
		final StringBuilder stringBuilder = new StringBuilder("CardReferenceDTO{");
		stringBuilder.append("id=").append(id);
		stringBuilder.append(", name='").append(name).append('\'');
		stringBuilder.append(", description='").append(description).append('\'');
		stringBuilder.append(", family='").append(family).append('\'');
		stringBuilder.append(", affinity='").append(affinity).append('\'');
		stringBuilder.append(", imgUrl='").append(imgUrl).append('\'');
		stringBuilder.append(", smallImgUrl='").append(smallImgUrl).append('\'');
		stringBuilder.append('}');
		return stringBuilder.toString();
	}
}
